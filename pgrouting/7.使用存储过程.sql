-- 2.3、练习6 —— 使用几何信息求方位角
WITH
dijkstra AS(
	SELECT * FROM pgr_dijkstra(
		'SELECT id, * FROM shenzhen_roads',
		9083, 9084
	)
),
get_geom AS (
	SELECT dijkstra.*, ways.name, ways.geom AS route_geom
	FROM dijkstra LEFT JOIN shenzhen_roads AS ways ON (edge = id)
	ORDER BY seq)
SELECT t.seq, t.name, t.cost, t.edge,
	st_startpoint(t.route_geom),
	st_endpoint(t.route_geom),
	v1.the_geom as start_point,
	v2.the_geom as end_point,
	-- calculating the azimuth
	degrees(ST_azimuth(v1.the_geom, v2.the_geom)) AS azimuth,
	ST_AsText(t.route_geom),
	t.route_geom
FROM get_geom t
left join shenzhen_roads r on t.edge = r.id
left join shenzhen_roads_vertices_pgr v1 on r.source = v1.id
left join shenzhen_roads_vertices_pgr v2 on r.target = v2.id
ORDER BY seq;

-- 2.4、练习7 —— 几何信息的方向性
WITH 
dijkstra AS(
	SELECT * FROM pgr_dijkstra(
		'SELECT id, * FROM shenzhen_roads',
		9083, 9084
	) 
),
get_geom AS (
    SELECT dijkstra.*, ways.name, 
	-- 将反向路段的几何信息逆序排列
    CASE
        WHEN dijkstra.node = ways.source THEN ways.geom
        ELSE ST_Reverse(ways.geom)
	END AS route_geom
    FROM dijkstra LEFT JOIN shenzhen_roads AS ways ON (edge = id)
    ORDER BY seq)
SELECT t.seq, t.name, t.cost,
    -- 计算方位角
    degrees(ST_azimuth(v1.the_geom, v2.the_geom)) AS azimuth,
    ST_AsText(t.route_geom),
    t.route_geom
FROM get_geom t
left join shenzhen_roads r on t.edge = r.id
left join shenzhen_roads_vertices_pgr v1 on r.source = v1.id
left join shenzhen_roads_vertices_pgr v2 on r.target = v2.id
ORDER BY seq;


-- 3.1、练习8 —— 创建一个函数
CREATE OR REPLACE FUNCTION wrk_dijkstra(
    IN edges_subset regclass,	-- 视图作为参数
    IN source BIGINT,
    IN target BIGINT,
    OUT seq INTEGER,
    OUT id BIGINT,
    OUT name TEXT,
    OUT cost FLOAT,
    OUT azimuth FLOAT,
    OUT route_readable TEXT,
    OUT route_geom geometry
) RETURNS SETOF record AS 
$BODY$
    WITH
    dijkstra AS (
        SELECT * FROM pgr_dijkstra(
            -- 使用参数化的视图
            'SELECT id, * FROM ' || $1,
            $2, $3)
    ),
    get_geom AS (
        SELECT dijkstra.*, ways.name,
            CASE
                WHEN dijkstra.node = ways.source THEN geom
                ELSE ST_Reverse(geom)
            END AS route_geom
        FROM dijkstra JOIN shenzhen_roads AS ways ON (edge = id)
        ORDER BY seq)
    SELECT
        t.seq, t.name, t.cost,
		-- 计算方位角
		degrees(ST_azimuth(v1.the_geom, v2.the_geom)) AS azimuth,
		ST_AsText(t.route_geom),
		t.route_geom
    FROM get_geom t
	left join shenzhen_roads r on t.edge = r.id
	left join shenzhen_roads_vertices_pgr v1 on r.source = v1.id
	left join shenzhen_roads_vertices_pgr v2 on r.target = v2.id
    ORDER BY seq;
$BODY$
LANGUAGE 'sql';

-- 使用函数
SELECT * FROM wrk_dijkstra('shenzhen_roads', 9083, 9084);

