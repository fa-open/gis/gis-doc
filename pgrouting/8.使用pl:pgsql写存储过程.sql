-- 一、规划从A点到B点的路径
-- 2.1、练习1 —— 顶点的数量
-- 和shenzhen_roads表相联系的顶点的数量
SELECT COUNT(*) FROM shenzhen_roads_vertices_pgr;

-- 和vehicle_net视图相联系的顶点的数量
SELECT COUNT(*) FROM shenzhen_roads_vertices_pgr
WHERE id IN (
	SELECT source FROM vehicle_net
	UNION
	SELECT target FROM vehicle_net
);

-- 和little_net视图相联系的顶点的数量
SELECT COUNT(*) FROM shenzhen_roads_vertices_pgr
WHERE id IN (
	SELECT source FROM little_net
	UNION
	SELECT target FROM little_net
);

-- 2.2、练习2 —— 最近顶点
-- 基于全部顶点计算
SELECT id 
FROM shenzhen_roads_vertices_pgr
ORDER BY the_geom <-> ST_SetSRID(ST_Point(12677354.9,2578172.3), 3857)
LIMIT 1;

-- 基于vehicle_net视图计算
WITH
vertices AS (
    SELECT * FROM shenzhen_roads_vertices_pgr
    WHERE id IN (
        SELECT source FROM vehicle_net
        UNION
        SELECT target FROM vehicle_net)
)
SELECT id FROM vertices
    ORDER BY the_geom <-> ST_SetSRID(ST_Point(12677354.9,2578172.3), 3857) LIMIT 1;

-- 基于little_net视图计算
WITH
vertices AS (
    SELECT * FROM shenzhen_roads_vertices_pgr
    WHERE id IN (
        SELECT source FROM little_net
        UNION
        SELECT target FROM little_net)
)
SELECT id FROM vertices
ORDER BY the_geom <-> ST_SetSRID(ST_Point(12677354.9,2578172.3), 3857) LIMIT 1;

-- 三、编写函数wrk_fromAtoB
-- 3.1、练习3 —— 创建函数
-- 创建函数wrk_fromAtoB：
CREATE OR REPLACE FUNCTION wrk_fromAtoB(
	IN edges_subset regclass,
	IN x1 NUMERIC, IN y1 numeric,
	IN x2 NUMERIC, IN y2 NUMERIC,
	OUT seq INTEGER,
	OUT id BIGINT,
	OUT name TEXT,
	OUT costs FLOAT,
	OUT azimuth FLOAT,
	OUT geom GEOMETRY
)
RETURNS SETOF record AS
$BODY$
DECLARE 
	final_query TEXT;
BEGIN
	final_query := 
		FORMAT($$
			WITH
			vertices AS (
				SELECT * FROM shenzhen_roads_vertices_pgr
				WHERE id IN (
					SELECT source FROM %1$I
					UNION
					SELECT target FROM %1$I
				)
			),
			dijkstra AS (
				SELECT * 
				FROM wrk_dijkstra(
					'%1$I',
					-- source
					(SELECT id FROM vertices
                        ORDER BY the_geom <-> ST_SetSRID(ST_Point(%2$s, %3$s), 3857) LIMIT 1),
					-- target
					(SELECT id FROM vertices
                        ORDER BY the_geom <-> ST_SetSRID(ST_Point(%4$s, %5$s), 3857) LIMIT 1)
				)
			)
			SELECT 
			   seq,
			   dijkstra.id as id,
			   dijkstra.name as name,
			   dijkstra.cost / 1000.0 AS costs,
			   azimuth,
			   route_geom AS geom
			FROM dijkstra 
			JOIN shenzhen_roads ways on dijkstra.id = ways.id;$$,
		edges_subset, x1,y1,x2,y2
		);
	RAISE notice '%', final_query; -- 执行该函数时显示函数的逻辑代码信息
	RETURN QUERY EXECUTE final_query;
END;
$BODY$
LANGUAGE 'plpgsql';

-- 3.2、练习4 —— 使用函数
-- 使用刚创建的函数wrk_fromAtoB计算从坐标点（12677354.9, 2578172.3）到坐标点（12677441.2, 2577908.3）的路径信息。
SELECT * FROM wrk_fromAtoB(
	'vehicle_net',
	12677354.9,2578172.3,
	12677441.2, 2577908.3
);

SELECT * FROM wrk_fromAtoB(
	'little_net',
	12677354.9,2578172.3,
	12677441.2, 2577908.3
);

SELECT * FROM wrk_fromAtoB(
	'shenzhen_roads',
	12677354.9,2578172.3,
	12677441.2, 2577908.3
);

