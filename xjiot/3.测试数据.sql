-- 8.0 在使用 ST_GeomFromText() 时，默认的坐标顺序是 (latitude, longitude)，而非常规的（longitude，latitude）
INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.92154712514704 42.08403248686871, 94.9215428225158 42.08292765468216)', 4236), 'path', 'road1', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.9215411316341 42.08292904647685, 94.92600909217785 42.08181779602831, 94.92650676697252 42.08112207797997, 94.92711494434778 42.08097849297117, 94.92713154338558 42.081187829408165)', 4236), 'path', 'road2', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.92154145655917 42.08292868477485, 94.91298821798065 42.081685650598295)', 4236), 'path', 'road3', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.91296026881759 42.08168678311622, 94.91414131395524 42.08023152068239)', 4236), 'path', 'road3', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.91413478503617 42.08022855914516, 94.91445762618882 42.08028700602113)', 4236), 'path', 'road3', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.91414452637582 42.080225075455104, 94.91711701410892 42.07683422745585)', 4236), 'path', 'road3', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.91712131772142 42.0768321533266, 94.91712792973989 42.076669977221734)', 4236), 'path', 'road3', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.91712131772142 42.0768321533266, 94.91749207157572 42.07684198218358)', 4236), 'path', 'road3', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.91712679804381 42.076666485250314, 94.91569070707375 42.07382097365242)', 4236), 'path', 'road3', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.91568830192335 42.0738227629142, 94.91564590408665 42.073726105633, 94.91563378695196 42.07362045698667)', 4236), 'path', 'road3', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.91563328340784 42.07361992643056, 94.91590350863265 42.073625657744)', 4236), 'path', 'road3', '', 'B', 1);

INSERT INTO gis_roads(geom, fclass, name, ref, oneway, penalty)
VALUES (st_geomfromtext('linestring(94.91563284061947 42.07361970699675, 94.91562671660043 42.073477335508805, 94.91607586570684 42.071506348256634)', 4236), 'path', 'road3', '', 'B', 1);

