-- 最短路径查询
WITH
dijkstra AS(
	SELECT * FROM pgr_dijkstra(
		'SELECT * FROM gis_roads',
		1, 3
	)
),
get_geom AS (
	SELECT dijkstra.*, ways.name, ways.geom AS route_geom
	FROM dijkstra LEFT JOIN gis_roads AS ways ON (edge = id)
	ORDER BY seq)
SELECT t.seq, t.name, t.cost, t.edge,
	st_startpoint(t.route_geom),
	st_endpoint(t.route_geom),
	ST_AsText(t.route_geom),
	t.route_geom
FROM get_geom t
ORDER BY seq;

-- 3.1、练习8 —— 创建一个函数
CREATE OR REPLACE FUNCTION wrk_dijkstra(
    IN edges_subset regclass,	-- 视图作为参数
    IN source BIGINT,
    IN target BIGINT,
    OUT seq INTEGER,
    OUT id BIGINT,
    OUT name TEXT,
    OUT cost FLOAT,
    OUT azimuth FLOAT,
    OUT route_readable TEXT,
    OUT route_geom geometry
) RETURNS SETOF record AS 
$BODY$
    WITH
    dijkstra AS (
        SELECT * FROM pgr_dijkstra(
            -- 使用参数化的视图
            'SELECT * FROM ' || $1,
            $2, $3)
    ),
    get_geom AS (
        SELECT dijkstra.*, ways.name,
            CASE
                WHEN dijkstra.node = ways.source THEN geom
                ELSE ST_Reverse(geom)
            END AS route_geom
        FROM dijkstra JOIN gis_roads AS ways ON (edge = id)
        ORDER BY seq)
    SELECT
        seq,
        edge,
        name,
        cost,
        degrees(ST_azimuth(ST_StartPoint(route_geom), ST_EndPoint(route_geom))) AS azimuth,
        ST_AsText(route_geom),
        route_geom
    FROM get_geom
    ORDER BY seq;
$BODY$
LANGUAGE 'sql';

-- 测试使用函数
SELECT * FROM wrk_dijkstra('gis_roads', 1, 3);

-- 建的函数wrk_fromAtoB计算从坐标点（lng, lat）到坐标点（lng, lat）的路径信息
CREATE OR REPLACE FUNCTION wrk_fromAtoB(
	IN edges_subset regclass,
	IN x1 NUMERIC, IN y1 numeric,
	IN x2 NUMERIC, IN y2 NUMERIC,
	OUT seq INTEGER,
	OUT id BIGINT,
	OUT name TEXT,
	OUT costs FLOAT,
	OUT azimuth FLOAT,
	OUT geom GEOMETRY
)
RETURNS SETOF record AS
$BODY$
DECLARE 
	final_query TEXT;
BEGIN
	final_query := 
		FORMAT($$
			WITH
			vertices AS (
				SELECT * FROM gis_roads_vertices_pgr
				WHERE id IN (
					SELECT source FROM %1$I
					UNION
					SELECT target FROM %1$I
				)
			),
			dijkstra AS (
				SELECT * 
				FROM wrk_dijkstra(
					'%1$I',
					-- source
					(SELECT id FROM vertices
                        ORDER BY the_geom <-> ST_SetSRID(ST_Point(%2$s, %3$s), 4236) LIMIT 1),
					-- target
					(SELECT id FROM vertices
                        ORDER BY the_geom <-> ST_SetSRID(ST_Point(%4$s, %5$s), 4236) LIMIT 1)
				)
			)
			SELECT 
			   seq,
			   dijkstra.id,
			   dijkstra.name,
			   dijkstra.cost AS costs,
			   azimuth,
			   route_geom AS geom
			FROM dijkstra 
			JOIN gis_roads ways USING(id);$$,
		edges_subset, x1,y1,x2,y2
		);
	RAISE notice '%', final_query; -- 执行该函数时显示函数的逻辑代码信息
	RETURN QUERY EXECUTE final_query;
END;
$BODY$
LANGUAGE 'plpgsql';


-- 测试函数
SELECT * FROM wrk_fromAtoB(
	'gis_roads',
	94.92154712514704, 42.08403248686871,
	94.91298821798065, 42.081685650598295
);

