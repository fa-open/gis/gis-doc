-- ①正向路径的成本：
UPDATE gis_roads
SET cost = ST_Length(geom), reverse_cost = -1
WHERE oneway = 'F';

-- ②反向路径的成本：
UPDATE gis_roads
SET reverse_cost = ST_Length(geom), cost = -1
WHERE oneway = 'T';

-- ③双向路径的成本：
UPDATE gis_roads
SET cost = ST_Length(geom), reverse_cost = ST_Length(geom)
WHERE oneway = 'B';

-- 如果需要重新构建拓扑，需要先将source、target设置为null。否则pgr_createTopology方法只会更新source、target为null的增量数据
update gis_roads set source = null, target = null;

-- 创建路网拓扑
SELECT pgr_createTopology(
	'gis_roads', 
	0.001,
	'geom',
	'id',
	'source',
	'target'
); 

