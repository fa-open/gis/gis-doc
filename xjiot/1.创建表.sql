-- 使用postgis插件
create extension postgis;

-- 使用pgrouting插件
create extension pgrouting;

CREATE SEQUENCE IF NOT EXISTS public.gis_roads_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 2147483647
    CACHE 1;

-- DROP TABLE public.gis_roads;
-- 创建路网表
CREATE TABLE IF NOT EXISTS public.gis_roads
(
    id integer NOT NULL DEFAULT nextval('gis_roads_id_seq'::regclass), -- ID
    geom geometry(LineString, 4236), -- 路网
    fclass character varying(28) COLLATE pg_catalog."default", -- 道路类型: path/footway
    name character varying(100) COLLATE pg_catalog."default", -- 道路名称
    ref character varying(20) COLLATE pg_catalog."default", -- 道路编码
    oneway character varying(1) COLLATE pg_catalog."default", -- 方向：B双向/F正向/T反向
    source integer, -- 路网edge起点
    target integer, -- 路网edge终点
    cost double precision, -- 路网花费成本
    reverse_cost double precision, -- 路网花费成本(反向)
    penalty double precision, -- 优先级，可用于乘以cost达到路径优先级控制
    CONSTRAINT shenzhen_roads_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

COMMENT ON TABLE public.gis_roads
    IS 'GIS-路网';
